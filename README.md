# L298N Module
This module is designed to be used with four GPIO pins on the Raspberry Pi.

## Usage
To use this module, create an instance of L298N using the four designated pins as 
attributes. Use the GPIO number as opposed to the board number.

```python
from L298N import L298N
motors = L298N(2, 3, 4, 5)
motors.stop()
```

## Contributions
If you see areas for improvement in this module, please submit an issue or 
feel free to email me. All contributions are welcome.
