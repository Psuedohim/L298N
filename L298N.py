import RPi.GPIO as io

class L298N():
    """A class to control an L298N dual H bridge motor driver."""
    def __init__(self, in1, in2, in3, in4):
        self.in1 = in1
        self.in2 = in2
        self.in3 = in3
        self.in4 = in4
        all_pins = [self.in1, self.in2, self.in3, self.in4]
        io.setmode(io.BCM)
        io.setup(all_pins, io.OUT)
        io.setwarnings(False)

    def left_forwards(self):
        io.output(self.in1, 1)
        io.output(self.in2, 0)
        io.output(self.in3, 0)
        io.output(self.in4, 0)

    def left_backwards(self):
        io.output(self.in1, 0)
        io.output(self.in2, 1)
        io.output(self.in3, 0)
        io.output(self.in4, 0)

    def right_forwards(self):
        io.output(self.in1, 0)
        io.output(self.in2, 0)
        io.output(self.in3, 1)
        io.output(self.in4, 0)

    def right_backwards(self):
        io.output(self.in1, 0)
        io.output(self.in2, 0)
        io.output(self.in3, 0)
        io.output(self.in4, 1)

    def rotate_left(self):
        io.output(self.in1, 1)
        io.output(self.in2, 0)
        io.output(self.in3, 0)
        io.output(self.in4, 1)

    def rotate_right(self):
        io.output(self.in1, 0)
        io.output(self.in2, 1)
        io.output(self.in3, 1)
        io.output(self.in4, 0)

    def forwards(self):
        io.output(self.in1, 1)
        io.output(self.in2, 0)
        io.output(self.in3, 1)
        io.output(self.in4, 0)

    def backwards(self):
        io.output(self.in1, 0)
        io.output(self.in2, 1)
        io.output(self.in3, 0)
        io.output(self.in4, 1)

    def stop(self):
        io.output(self.in1, 0)
        io.output(self.in2, 0)
        io.output(self.in3, 0)
        io.output(self.in4, 0)

    def cleanup(self):
        io.cleanup()
        io.setmode(io.BCM)
